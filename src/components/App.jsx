import React , {Component} from 'react';
import ReactDom from 'react-dom';
import Search from './github/Search.jsx';
import Profile from './github/Profile.jsx';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userName: 'owdlevi',
      userData: [],
      userRepos: [],
      perPage: 5
    }
  }

  // Get user fata from github

  getUserData() {
    $.ajax({
      url: 'https://api.github.com/users/' + this.state.userName + '?client_id=' + this.props.clientId+ '&client_secret='+this.props.clientSecret,
      dataType: 'json',
      cache: false,
      success: function(data) {
        this.setState({userData: data});
      }.bind(this),
      error: function(xhr, status, err) {
        this.setState({username: null});
        console.log(err);
      }.bind(this)
    })
  }

  // Get user repos
  getUserRepos() {
    $.ajax({
      url: 'https://api.github.com/users/' + this.state.userName + '/repos?per_page=' + this.state.perPage + 'client_id=' + this.props.clientId+ '&client_secret='+this.props.clientSecret+'&sort=created',
      dataType: 'json',
      cache: false,
      success: function(data) {
        this.setState({userRepos: data});
      }.bind(this),
      error: function(xhr, status, err) {
        this.setState({username: null});
        console.log(err);
      }.bind(this)
    })
  }

 handleFormSubmit(username) {
    this.setState({userName: username} , function(){
      this.getUserData();
      this.getUserRepos();
    });
 }

  componentDidMount() {
    this.getUserData();
    this.getUserRepos();
  }

  render() {
    return (
      <div>
          <Search onFormSubmit = {this.handleFormSubmit.bind(this)} />
          <Profile {...this.state} />
      </div>
    )
  }
}

App.propTypes = {
  clientId: React.PropTypes.string,
  clientSecret: React.PropTypes.string
}

App.defaultProps = {
  clientId: '139237614219c6649967',
  clientSecret: 'cc08bec482f922acdbd94d9f967c71606081bf5f'
}

export default App
